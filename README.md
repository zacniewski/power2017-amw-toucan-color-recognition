# README #

System przetwarzania obrazów w przeglądarce

### Cel projektu ###

* Realizacja rozpoznania kolorów w przeglądarce internetowej

### Technologie ###

* tracking.js, HTML

### Autorzy ###

* Maciej Bruski
* Rafał Zuchora

### Link ###

* https://www.zacniewski.pl/vision/
